project :renderer => :haml, :stylesheet => :sass, :script => :jquery, :orm => :activerecord, :adapter => :sqlite, :tiny => :tiny, :dev => :dev
# views / layouts
get 'https://github.com/lenzcom/960mock/raw/master/haml/demo_24_col.haml','app/views/layouts/demo_24_col.haml'
get 'https://github.com/lenzcom/960mock/raw/master/haml/demo.haml','app/views/layouts/demo.haml'
get 'https://github.com/lenzcom/960mock/raw/master/haml/rtl_demo.haml','app/views/layouts/rtl_demo.haml'
get 'https://github.com/lenzcom/960mock/raw/master/haml/rtl_demo_24_col.haml','app/views/layouts/rtl_demo_24_col.haml'
# sass / src css files
get 'https://github.com/lenzcom/960mock/raw/master/sass/960.sass','app/stylesheets/960.sass'
get 'https://github.com/lenzcom/960mock/raw/master/sass/960_24_col.sass','app/stylesheets/960_24_col.sass'
get 'https://github.com/lenzcom/960mock/raw/master/sass/demo.sass','app/stylesheets/demo.sass'
get 'https://github.com/lenzcom/960mock/raw/master/sass/reset.sass','app/stylesheets/reset.sass'
get 'https://github.com/lenzcom/960mock/raw/master/sass/rtl_960.sass','app/stylesheets/rtl_960.sass'
get 'https://github.com/lenzcom/960mock/raw/master/sass/rtl_960_24_col.sass','app/stylesheets/rtl_960_24_col.sass'
get 'https://github.com/lenzcom/960mock/raw/master/sass/rtl_text.sass','app/stylesheets/rtl_text.sass'
get 'https://github.com/lenzcom/960mock/raw/master/sass/text.sass','app/stylesheets/text.sass'
# grid background images
get 'https://github.com/lenzcom/960mock/raw/master/images/12_col.gif','public/images/12_col.gif'
get 'https://github.com/lenzcom/960mock/raw/master/images/16_col.gif','public/images/16_col.gif'
get 'https://github.com/lenzcom/960mock/raw/master/images/24_col.gif','public/images/24_col.gif'
get 'https://github.com/lenzcom/960mock/raw/master/images/12_col.png','public/images/12_col.png'
get 'https://github.com/lenzcom/960mock/raw/master/images/16_col.png','public/images/16_col.png'
get 'https://github.com/lenzcom/960mock/raw/master/images/24_col.png','public/images/24_col.png'
# routes
ROUTES = <<RUBY

  # use demo layout
  layout  :demo
  
  # index route
  get '/' do
    haml "960 Grid System"
  end
  
RUBY
inject_into_file 'app/app.rb', ROUTES, :after => "render 'errors/404'\n  #   end\n  #"